<?php require_once 'farmacia.php'; ?>

<?php
ob_start();
?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<table>
        <thead>
          <tr>
              <td>id</td>
              <td>Medicamento</td>
              <td>Lote</td>
              <td>Vencimiento</td>
              <td>Restante</td>
              <td>Total</td>
              <td>Ubicacion</td>
            </tr>
        </thead>
     <tbody>
<?php

$query = "SELECT * FROM farmacia";
$result = mysqli_query($conn, $query);

 while($row = mysqli_fetch_array($result)): ?>
        <tr>
            <th><?php echo $id=$row["id_farmacia"]; ?></th>
            <th><?php echo $medicamento=$row["medicamento"]; ?></th>
            <th><?php echo $lote=$row["lote"]; ?></th>
            <th><?php echo $vencimiento=$row["vencimiento"]; ?></th>
            <th><?php echo $restante=$row["restante"]; ?></th>
            <th><?php echo $total=$row["total"]; ?></th>
            <th><?php echo $ubicacion=$row["ubicacion"]; ?></th> 
        <?php endwhile; ?>
         </tr>
  </tbody>
 </table>
</body>
</html>

<?php

$html = ob_get_clean();

require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
$dompdf = new Dompdf();
$dompdf->loadHtml(utf8_decode($html));
$dompdf->setPaper('letter');
$dompdf->render();
$dompdf->stream("farmacia-medicamentos.pdf", array("Attachment" => true));


 ?>

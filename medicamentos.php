<?php require_once 'includes/header.php'; ?>

<?php
include('farmacia.php');
include('includes/prohibiracceso.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Farmacia</title>
</head>
<body>

<?php require_once 'includes/menu.php'; ?>

</header>
<br/><br/>
<div class="clearfix"> </div><br/>
    <a class="btn btn-primary"  href="SC2.php">AGREGAR</a>
    <a class="btn btn-success"  href="reporteEXCEL.php">DESCARGAR EXCEL</a>
    <a class="btn btn-danger"  href="pdf.php">DESCARGAR PDF</a>
    <div class="clearfix"> </div>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Medicamento</th> 
                <th scope="col">Lote</th> 
                <th scope="col">Vencimiento</th> 
                <th scope="col">Restante</th> 
                <th scope="col">Total</th> 
                <th scope="col">Ubicacion</th>
                <th scope="col">Acciones</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
    <tbody>
    <?php 
    $query = "SELECT * FROM farmacia";
    $result = mysqli_query($conn,$query);
    while($row = mysqli_fetch_array($result)): ?>
        <tr>
            <th><?php echo $id=$row["id_farmacia"]; ?></th>
            <th><?php echo $medicamento=$row["medicamento"]; ?></th>
            <th><?php echo $lote=$row["lote"]; ?></th>
            <th><?php echo $vencimiento=$row["vencimiento"]; ?></th>
            <th><?php echo $restante=$row["restante"]; ?></th>
            <th><?php echo $total=$row["total"]; ?></th>
            <th><?php echo $ubicacion=$row["ubicacion"]; ?></th>
            <th><a class="eliminar btn btn-danger" href="eliminar.php?id_farmacia=<?php echo $row['id_farmacia'];?>">Eliminar</th> 
            <th>
            <a href="editar.php? 
                    id_farmacia=<?php echo $row['id_farmacia'];?>  &
                    medicamento=<?php echo $row['medicamento'];?> &
                    lote=<?php echo $row['lote'];?> &
                    vencimiento=<?php echo $row['vencimiento'];?> &
                    restante=<?php echo $row['restante'];?> &
                    total=<?php echo $row['total'];?> &
                    ubicacion=<?php echo $row['ubicacion'];?>
                    " class="btn btn-primary">Editar</a>
            </th>
        <?php endwhile; ?>
    </tbody>
    </table>
    <header> 
</body>
</html>
<?php require_once 'includes/footer.php'; ?>



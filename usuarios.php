<?php require_once 'includes/header.php'; ?>

<?php
include('farmacia.php');
include('includes/prohibiracceso.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php require_once 'includes/menu.php'; ?>
<br/><br/><br/>
<br/>
<div class="container">
    <div class="row justify-content-center align-items-center g-2">
        <div class="col"></div>
        <div class="col">
            
        <div class="card">
        <div class="card-header">
           
        </div>
        <div class="card-body">
            
        <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">USUARIO</th> 
                <th scope="col">PASSWORD</th>
                <th scope="col">ACCIONES</th> 
            </tr>
        </thead>
    <tbody>
    <?php 
    $query = "SELECT * FROM users";
    $result = mysqli_query($conn,$query);
    while($row = mysqli_fetch_array($result)): ?>
        <tr>
            <th><?php echo $id=$row["id"]; ?></th>
            <th><?php echo $name=$row["name"]; ?></th>
            <th><?php echo $password=$row["password"]; ?></th>
            
            <th><a class="eliminar btn btn-danger" href="eliminar_usuario.php?id=<?php echo $row['id'];?>">Eliminar</th> 
        <?php endwhile; ?>
    </tbody>
    </table>

        </div>
        <div class="card-footer text-muted">
            
        </div>
    </div>
    
        </div>
        <div class="col"></div>
    </div>
</div>


    
    
</body>
</html>
<?php require_once 'includes/footer.php'; ?>